﻿using System;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using static System.Windows.Forms.ListViewItem;
using static ExploradordeArchivos.FileManager;

namespace ExploradordeArchivos
{
    public partial class VentanaPrincipal : Form
    {
        //
        // Atributos
        //
        private static string CARPETA_INICIAL = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        private bool MostrarHintBusqueda = true;
        private string DirectorioActual = CARPETA_INICIAL;

        //
        // Constructor
        //
        public VentanaPrincipal()
        {
            InitializeComponent();
            EstablecerTreeView();
        }

        //
        // Otros métodos
        //
        private void EstablecerTreeView()
        {
            TreeNode rootNode;
            DirectoryInfo info = new DirectoryInfo(CARPETA_INICIAL);

            if (info.Exists)
            {
                rootNode = new TreeNode(info.Name);
                rootNode.Tag = info;
                GetDirectories(info.GetDirectories(), rootNode);
                PanelArbolDirectorio.Nodes.Add(rootNode);

                // Simulate onClick() to show content at first.
                PanelArbol_NodeMouseClick(new object(),
                                        new TreeNodeMouseClickEventArgs(
                                            rootNode,
                                            MouseButtons.Left,
                                            0,
                                            0,
                                            0 )
                                        );
            }
        }
        private void GetDirectories(DirectoryInfo[] ChildDirs, TreeNode ParentNode)
        {
            TreeNode ChildNode;
            DirectoryInfo[] SubChildDirs;

            foreach (DirectoryInfo subDir in ChildDirs)
            {
                try
                {
                    ChildNode = new TreeNode(subDir.Name, 0, 0);
                    ChildNode.Tag = subDir;
                    ChildNode.ImageKey = "Folder";
                    SubChildDirs = subDir.GetDirectories();

                    if (SubChildDirs.Length != 0)
                        GetDirectories(SubChildDirs, ChildNode);

                    ParentNode.Nodes.Add(ChildNode);

                }
                catch (UnauthorizedAccessException) { }
                
            }
        }

        private int GetImageFileIndexByExtension(string FilePath)
        {
            if(!string.IsNullOrEmpty(FilePath.Trim()) &&
                FileAttributes.Directory != File.GetAttributes(FilePath))
            {

                switch (Path.GetExtension(FilePath))
                {
                    case ".txt":
                        return 1;
                    case ".pdf":
                        return 2;
                    case ".exe":
                        if (HasIcon(FilePath))
                            return ImageListBig.Images.IndexOfKey(GenerateKeyForIcon(FilePath));
                        return 3; // Icono por defecto para los archivos .EXE
                    case ".png":
                    case ".jpg":
                    case ".jpeg":
                    case ".bmp":
                    case ".gif":
                        try
                        {
                            AddIconToListsFromFile(FilePath, GetImageFileThumbnail(FilePath));
                            return ImageListBig.Images.IndexOfKey(GenerateKeyForIcon(FilePath));
                        } catch(NullReferenceException) { return 4; } // Icono por defecto para imágenes
                    case ".mp4":
                    case ".avi":
                    case ".mkv":
                    case ".flv":
                    case ".wmv":
                        return 5;
                    case ".rar":
                    case ".zip":
                    case ".7z":
                        return 6;
                    default:
                        return 1;
                }
            }

            return 0;
            
        }

        private void AddIconToListsFromFile(string FilePath)
        {
            AddIconToListsFromFile(FilePath, GetIcon(FilePath));
        }
        private void AddIconToListsFromFile(string FilePath, Icon icon)
        {
            ImageListSmall.Images.Add(GenerateKeyForIcon(FilePath), icon);
            ImageListBig.Images.Add(GenerateKeyForIcon(FilePath), icon);
        }

        /**
         * Mediante el nombre de archivo, averigua el tipo. Si no se soporta o
         * contempla, retorna "Archivo", que es el valor por defecto.
         */
        private string GetFileTypeName(string FileFullName)
        {
            switch(Path.GetExtension(FileFullName))
            {
                case ".txt":
                    return "Archivo de texto";
                case ".pdf":
                    return "Documento PDF";
                case ".xml":
                    return "Archivo de Marcado XML";
                case ".exe":
                    return "Aplicación de Windows";
                case ".rar":
                case ".zip":
                case ".7z":
                    return "Archivo comprimido en " + Path.GetExtension(FileFullName).Replace(".", "").ToUpper();
                case ".png":
                case ".jpg":
                case ".jpeg":
                case ".bmp":
                case ".gif":
                    return "Imagen " + Path.GetExtension(FileFullName).Replace(".", "").ToUpper();
                case ".mp4":
                case ".avi":
                case ".mkv":
                case ".flv":
                case ".wmv":
                    return "Archivo de vídeo " + Path.GetExtension(FileFullName).Replace(".","").ToUpper();
                default:
                    return "Archivo";
            }
        }

        /**
         * Permite que el usuario introduzca expresiones Regex para búsqueda,
         * y por el contrario, lo más conocido en Windows (*) para "todos los archivos y carpetas".
         * @param nombre El nombre de archivo a buscar.
         */
        private void MarcarCoincidencias(string nombre)
        {
            // (?i) Indica el comienzo de una cadena case-insensitive y (?-i) el final.
            string WindowsRegex = "(?i)" + nombre.Replace(@"\", @"\\")
                                        .Replace(".", "\\.")
                                        .Replace("*", ".")
                                        .Replace("?", "[A-z0-9]{0,1}" +
                                        "(?-i)");

            // Buscar archivo por archivo el nombre
            foreach(ListViewItem item in ListadoArchivos.Items)
            {
                try
                {
                    if (!nombre.Trim().Equals("") && Regex.IsMatch(item.Text.ToLower(), WindowsRegex))
                        item.BackColor = Color.LightSkyBlue;
                    else
                        item.BackColor = Color.Transparent;
                } catch(ArgumentException) { }
                
            }
        }

        //
        // Listeners
        //
        private void PanelArbol_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {

            // Establecer ancho de las columnas
            foreach (ColumnHeader col in ListadoArchivos.Columns)
                col.Width = -2;

            e.Node.Expand();
            ListadoArchivos.Items.Clear();

            // Cargar datos
            DirectoryInfo SelectedNodeInfo = e.Node.Tag as DirectoryInfo;

            // Actualizar directorio actual
            DirectorioActual = SelectedNodeInfo.FullName + "\\";

            ListViewSubItem[] SelectedNode_Childs;
            ListViewItem ChildItem = null;

            // Cargar Carpetas
            foreach (DirectoryInfo child in SelectedNodeInfo.GetDirectories())
            {
                ChildItem = new ListViewItem(child.Name, 0);
                SelectedNode_Childs = new ListViewSubItem[] {
                    new ListViewSubItem(ChildItem, "Carpeta"),
                    new ListViewSubItem(ChildItem,child.LastAccessTime.ToShortDateString())
                };
                ChildItem.SubItems.AddRange(SelectedNode_Childs);
                ListadoArchivos.Items.Add(ChildItem);
            }

            // Cargar Archivos
            foreach (FileInfo file in SelectedNodeInfo.GetFiles())
            {
                if (HasIcon(file.FullName) && file.Extension.Equals(".exe"))
                    AddIconToListsFromFile(file.FullName);

                ChildItem = new ListViewItem(file.Name, GetImageFileIndexByExtension(file.FullName));
                SelectedNode_Childs = new ListViewSubItem[] {
                    new ListViewSubItem(ChildItem, GetFileTypeName(file.FullName)),
                    new ListViewSubItem(ChildItem,file.LastAccessTime.ToShortDateString())
                };
                ChildItem.SubItems.AddRange(SelectedNode_Childs);
                ChildItem.ToolTipText = GetSizeString(file.Length);
                ListadoArchivos.Items.Add(ChildItem);
            }
            ListadoArchivos.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

        }

        // Listeners tipos de vista

        // Iconos (General)
        private void MenuItemIconos_Click(object sender, EventArgs e)
        {
            this.MenuItemIconos.Checked = true;
            this.MenuItemIconosPeq.Checked = true;
            this.MenuItemLista.Checked = false;
            this.MenuItemDetalle.Checked = false;
            this.MenuItemLista.Checked = false;
            this.MenuItemDetalle.Checked = false;
            this.MenuItemCuadricula.Checked = false;

            this.ListadoArchivos.View = View.SmallIcon;
        }

        // Iconos pequeños
        private void MenuItemIconosPeq_Click(object sender, EventArgs e)
        {
            this.MenuItemIconos.Checked = true;
            this.MenuItemIconosPeq.Checked = true;
            this.MenuItemLista.Checked = false;
            this.MenuItemDetalle.Checked = false;
            this.MenuItemIconosGran.Checked = false;
            this.MenuItemCuadricula.Checked = false;

            this.ListadoArchivos.View = View.SmallIcon;
        }

        // Iconos grandes
        private void MenuItemIconosGran_Click(object sender, EventArgs e)
        {
            this.MenuItemIconos.Checked = true;
            this.MenuItemIconosGran.Checked = true;
            this.MenuItemLista.Checked = false;
            this.MenuItemDetalle.Checked = false;
            this.MenuItemIconosPeq.Checked = false;
            this.MenuItemCuadricula.Checked = false;

            this.ListadoArchivos.View = View.LargeIcon;
        }

        // Lista
        private void MenuItemLista_Click(object sender, EventArgs e)
        {
            this.MenuItemLista.Checked = true;
            this.MenuItemDetalle.Checked = false;
            this.MenuItemIconos.Checked = false;
            this.MenuItemIconosPeq.Checked = false;
            this.MenuItemIconosGran.Checked = false;
            this.MenuItemCuadricula.Checked = false;

            this.ListadoArchivos.View = View.List;
        }

        // Detalle
        private void MenuItemDetalle_Click(object sender, EventArgs e)
        {
            this.MenuItemDetalle.Checked = true;
            this.MenuItemLista.Checked = false;
            this.MenuItemIconos.Checked = false;
            this.MenuItemIconosPeq.Checked = false;
            this.MenuItemIconosGran.Checked = false;
            this.MenuItemCuadricula.Checked = false;

            this.ListadoArchivos.View = View.Details;
            ListadoArchivos.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        // Cuadrícula
        private void MenuItemCuadricula_Click(object sender, EventArgs e)
        {
            this.MenuItemDetalle.Checked = false;
            this.MenuItemLista.Checked = false;
            this.MenuItemIconos.Checked = false;
            this.MenuItemIconosPeq.Checked = false;
            this.MenuItemIconosGran.Checked = false;
            this.MenuItemCuadricula.Checked = true;

            this.ListadoArchivos.View = View.Tile;
        }

        private void TextoBusqueda_TextChanged(object sender, EventArgs e)
        {
            MarcarCoincidencias(TextoBusqueda.Text);
        }


        private void TextoBusqueda_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TextoBusqueda.Text.Trim()))
            {
                TextoBusqueda.Text = "Búsqueda";
                TextoBusqueda.ForeColor = Color.Gray;

                MostrarHintBusqueda = true;
            }
        }

        private void TextoBusqueda_MouseDown(object sender, MouseEventArgs e)
        {
            if (MostrarHintBusqueda)
            {
                TextoBusqueda.Text = "";
                TextoBusqueda.ForeColor = Color.Black;
            }

            MostrarHintBusqueda = false;
        }

        // Ejecuta/abre el archivo si es que no es un directorio, en caso contrario,
        // lo abre en el menú lateral y carga su contenido.
        private void ListadoArchivos_ItemActivate(object sender, EventArgs e)
        {
            // Obtener item/s clicados
            ListView lista = sender as ListView;

            foreach (ListViewItem i in lista.SelectedItems)
            {
                if(File.GetAttributes(DirectorioActual + i.Text) != FileAttributes.Directory) // Es un archivo
                    System.Diagnostics.Process.Start(DirectorioActual + i.Text);
                else
                    foreach(TreeNode node in PanelArbolDirectorio.SelectedNode.Nodes)
                    {
                        if (node.Text.Equals(i.Text))
                            PanelArbol_NodeMouseClick(new object(),
                                new TreeNodeMouseClickEventArgs(node,MouseButtons.Left,0,0,0)
                                );
                    }
            }

        }

        // Orden de elementos
        private void MenuItemOrdenNone_Click(object sender, EventArgs e)
        {
            MenuItemOrdenAsc.Checked = false;
            MenuItemOrdenDesc.Checked = false;
            MenuItemOrdenNone.Checked = true;

            ListadoArchivos.Sorting = SortOrder.None;
            ListadoArchivos.Sort();
        }

        private void MenuItemOrdenDesc_Click(object sender, EventArgs e)
        {
            MenuItemOrdenAsc.Checked = false;
            MenuItemOrdenNone.Checked = false;
            MenuItemOrdenDesc.Checked = true;

            ListadoArchivos.Sorting = SortOrder.Descending;
        }

        private void MenuItemOrdenAsc_Click(object sender, EventArgs e)
        {
            MenuItemOrdenNone.Checked = false;
            MenuItemOrdenDesc.Checked = false;
            MenuItemOrdenAsc.Checked = true;

            ListadoArchivos.Refresh();
        }

    }
}
