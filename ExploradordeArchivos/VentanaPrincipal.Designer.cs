﻿using System.Drawing;

namespace ExploradordeArchivos
{
    partial class VentanaPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VentanaPrincipal));
            this.PanelMenu = new System.Windows.Forms.ToolStripContainer();
            this.PanelSeparador = new System.Windows.Forms.SplitContainer();
            this.PanelArbolDirectorio = new System.Windows.Forms.TreeView();
            this.ImageListSmall = new System.Windows.Forms.ImageList(this.components);
            this.ListadoArchivos = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ImageListBig = new System.Windows.Forms.ImageList(this.components);
            this.LabelCargando = new System.Windows.Forms.Label();
            this.BaraMenu = new System.Windows.Forms.MenuStrip();
            this.MenunArchivo = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemSalir = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuVista = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemIconos = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemIconosPeq = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemIconosGran = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemLista = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemDetalle = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemCuadricula = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuItemOrdenacion = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemOrdenAsc = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemOrdenDesc = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuItemOrdenNone = new System.Windows.Forms.ToolStripMenuItem();
            this.TextoBusqueda = new System.Windows.Forms.ToolStripTextBox();
            this.PanelMenu.ContentPanel.SuspendLayout();
            this.PanelMenu.TopToolStripPanel.SuspendLayout();
            this.PanelMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PanelSeparador)).BeginInit();
            this.PanelSeparador.Panel1.SuspendLayout();
            this.PanelSeparador.Panel2.SuspendLayout();
            this.PanelSeparador.SuspendLayout();
            this.BaraMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // PanelMenu
            // 
            // 
            // PanelMenu.ContentPanel
            // 
            this.PanelMenu.ContentPanel.Controls.Add(this.PanelSeparador);
            this.PanelMenu.ContentPanel.Size = new System.Drawing.Size(697, 299);
            this.PanelMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelMenu.LeftToolStripPanelVisible = false;
            this.PanelMenu.Location = new System.Drawing.Point(0, 0);
            this.PanelMenu.Name = "PanelMenu";
            this.PanelMenu.RightToolStripPanelVisible = false;
            this.PanelMenu.Size = new System.Drawing.Size(697, 326);
            this.PanelMenu.TabIndex = 0;
            this.PanelMenu.Text = "toolStripContainer1";
            // 
            // PanelMenu.TopToolStripPanel
            // 
            this.PanelMenu.TopToolStripPanel.Controls.Add(this.BaraMenu);
            // 
            // PanelSeparador
            // 
            this.PanelSeparador.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelSeparador.Location = new System.Drawing.Point(0, 0);
            this.PanelSeparador.Name = "PanelSeparador";
            // 
            // PanelSeparador.Panel1
            // 
            this.PanelSeparador.Panel1.Controls.Add(this.PanelArbolDirectorio);
            // 
            // PanelSeparador.Panel2
            // 
            this.PanelSeparador.Panel2.Controls.Add(this.ListadoArchivos);
            this.PanelSeparador.Panel2.Controls.Add(this.LabelCargando);
            this.PanelSeparador.Size = new System.Drawing.Size(697, 299);
            this.PanelSeparador.SplitterDistance = 232;
            this.PanelSeparador.TabIndex = 0;
            // 
            // PanelArbolDirectorio
            // 
            this.PanelArbolDirectorio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelArbolDirectorio.ImageIndex = 0;
            this.PanelArbolDirectorio.ImageList = this.ImageListSmall;
            this.PanelArbolDirectorio.Location = new System.Drawing.Point(0, 0);
            this.PanelArbolDirectorio.Name = "PanelArbolDirectorio";
            this.PanelArbolDirectorio.SelectedImageIndex = 0;
            this.PanelArbolDirectorio.Size = new System.Drawing.Size(232, 299);
            this.PanelArbolDirectorio.TabIndex = 0;
            this.PanelArbolDirectorio.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.PanelArbol_NodeMouseClick);
            // 
            // ImageListSmall
            // 
            this.ImageListSmall.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageListSmall.ImageStream")));
            this.ImageListSmall.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageListSmall.Images.SetKeyName(0, "Folder");
            this.ImageListSmall.Images.SetKeyName(1, "File");
            this.ImageListSmall.Images.SetKeyName(2, "Pdf");
            this.ImageListSmall.Images.SetKeyName(3, "Exe");
            this.ImageListSmall.Images.SetKeyName(4, "Img");
            this.ImageListSmall.Images.SetKeyName(5, "Video");
            this.ImageListSmall.Images.SetKeyName(6, "Zip");
            // 
            // ListadoArchivos
            // 
            this.ListadoArchivos.Activation = System.Windows.Forms.ItemActivation.TwoClick;
            this.ListadoArchivos.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.ListadoArchivos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListadoArchivos.FullRowSelect = true;
            this.ListadoArchivos.GridLines = true;
            this.ListadoArchivos.HideSelection = false;
            this.ListadoArchivos.LargeImageList = this.ImageListBig;
            this.ListadoArchivos.Location = new System.Drawing.Point(0, 0);
            this.ListadoArchivos.Name = "ListadoArchivos";
            this.ListadoArchivos.ShowItemToolTips = true;
            this.ListadoArchivos.Size = new System.Drawing.Size(461, 299);
            this.ListadoArchivos.SmallImageList = this.ImageListSmall;
            this.ListadoArchivos.TabIndex = 0;
            this.ListadoArchivos.UseCompatibleStateImageBehavior = false;
            this.ListadoArchivos.View = System.Windows.Forms.View.List;
            this.ListadoArchivos.ItemActivate += new System.EventHandler(this.ListadoArchivos_ItemActivate);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Nombre";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Tipo";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Última Modificación";
            // 
            // ImageListBig
            // 
            this.ImageListBig.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageListBig.ImageStream")));
            this.ImageListBig.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageListBig.Images.SetKeyName(0, "Folder");
            this.ImageListBig.Images.SetKeyName(1, "File");
            this.ImageListBig.Images.SetKeyName(2, "Pdf");
            this.ImageListBig.Images.SetKeyName(3, "Exe");
            this.ImageListBig.Images.SetKeyName(4, "Img");
            this.ImageListBig.Images.SetKeyName(5, "Video");
            this.ImageListBig.Images.SetKeyName(6, "Zip");
            // 
            // LabelCargando
            // 
            this.LabelCargando.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.LabelCargando.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LabelCargando.Location = new System.Drawing.Point(0, 0);
            this.LabelCargando.Name = "LabelCargando";
            this.LabelCargando.Size = new System.Drawing.Size(461, 299);
            this.LabelCargando.TabIndex = 1;
            this.LabelCargando.Text = "Cargando...";
            this.LabelCargando.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BaraMenu
            // 
            this.BaraMenu.Dock = System.Windows.Forms.DockStyle.None;
            this.BaraMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenunArchivo,
            this.MenuVista,
            this.TextoBusqueda});
            this.BaraMenu.Location = new System.Drawing.Point(0, 0);
            this.BaraMenu.Name = "BaraMenu";
            this.BaraMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.BaraMenu.Size = new System.Drawing.Size(697, 27);
            this.BaraMenu.TabIndex = 0;
            this.BaraMenu.Text = "menuStrip1";
            // 
            // MenunArchivo
            // 
            this.MenunArchivo.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItemSalir});
            this.MenunArchivo.Name = "MenunArchivo";
            this.MenunArchivo.Size = new System.Drawing.Size(60, 23);
            this.MenunArchivo.Text = "Archivo";
            // 
            // MenuItemSalir
            // 
            this.MenuItemSalir.Image = global::ExploradordeArchivos.Properties.Resources.Close_red_16x;
            this.MenuItemSalir.Name = "MenuItemSalir";
            this.MenuItemSalir.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.MenuItemSalir.Size = new System.Drawing.Size(138, 22);
            this.MenuItemSalir.Text = "Salir";
            // 
            // MenuVista
            // 
            this.MenuVista.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItemIconos,
            this.MenuItemLista,
            this.MenuItemDetalle,
            this.MenuItemCuadricula,
            this.toolStripSeparator1,
            this.MenuItemOrdenacion});
            this.MenuVista.Name = "MenuVista";
            this.MenuVista.Size = new System.Drawing.Size(44, 23);
            this.MenuVista.Text = "Vista";
            // 
            // MenuItemIconos
            // 
            this.MenuItemIconos.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItemIconosPeq,
            this.MenuItemIconosGran});
            this.MenuItemIconos.Image = global::ExploradordeArchivos.Properties.Resources.icon_editor_picture;
            this.MenuItemIconos.Name = "MenuItemIconos";
            this.MenuItemIconos.Size = new System.Drawing.Size(145, 22);
            this.MenuItemIconos.Text = "Iconos";
            this.MenuItemIconos.Click += new System.EventHandler(this.MenuItemIconos_Click);
            // 
            // MenuItemIconosPeq
            // 
            this.MenuItemIconosPeq.Name = "MenuItemIconosPeq";
            this.MenuItemIconosPeq.Size = new System.Drawing.Size(126, 22);
            this.MenuItemIconosPeq.Text = "Pequeños";
            this.MenuItemIconosPeq.Click += new System.EventHandler(this.MenuItemIconosPeq_Click);
            // 
            // MenuItemIconosGran
            // 
            this.MenuItemIconosGran.Name = "MenuItemIconosGran";
            this.MenuItemIconosGran.Size = new System.Drawing.Size(126, 22);
            this.MenuItemIconosGran.Text = "Grandes";
            this.MenuItemIconosGran.Click += new System.EventHandler(this.MenuItemIconosGran_Click);
            // 
            // MenuItemLista
            // 
            this.MenuItemLista.Checked = true;
            this.MenuItemLista.CheckState = System.Windows.Forms.CheckState.Checked;
            this.MenuItemLista.Image = global::ExploradordeArchivos.Properties.Resources.BulletList_16x;
            this.MenuItemLista.Name = "MenuItemLista";
            this.MenuItemLista.Size = new System.Drawing.Size(145, 22);
            this.MenuItemLista.Text = "Lista";
            this.MenuItemLista.Click += new System.EventHandler(this.MenuItemLista_Click);
            // 
            // MenuItemDetalle
            // 
            this.MenuItemDetalle.Image = global::ExploradordeArchivos.Properties.Resources.DetailDataView_16x;
            this.MenuItemDetalle.Name = "MenuItemDetalle";
            this.MenuItemDetalle.Size = new System.Drawing.Size(145, 22);
            this.MenuItemDetalle.Text = "Detalle";
            this.MenuItemDetalle.Click += new System.EventHandler(this.MenuItemDetalle_Click);
            // 
            // MenuItemCuadricula
            // 
            this.MenuItemCuadricula.Image = global::ExploradordeArchivos.Properties.Resources.GridLayoutDiv_16x_32;
            this.MenuItemCuadricula.Name = "MenuItemCuadricula";
            this.MenuItemCuadricula.Size = new System.Drawing.Size(145, 22);
            this.MenuItemCuadricula.Text = "Cuadrícula";
            this.MenuItemCuadricula.Click += new System.EventHandler(this.MenuItemCuadricula_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(142, 6);
            // 
            // MenuItemOrdenacion
            // 
            this.MenuItemOrdenacion.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItemOrdenAsc,
            this.MenuItemOrdenDesc,
            this.toolStripSeparator2,
            this.MenuItemOrdenNone});
            this.MenuItemOrdenacion.Name = "MenuItemOrdenacion";
            this.MenuItemOrdenacion.Size = new System.Drawing.Size(145, 22);
            this.MenuItemOrdenacion.Text = "Ordenación...";
            // 
            // MenuItemOrdenAsc
            // 
            this.MenuItemOrdenAsc.Name = "MenuItemOrdenAsc";
            this.MenuItemOrdenAsc.Size = new System.Drawing.Size(142, 22);
            this.MenuItemOrdenAsc.Text = "Ascendente";
            this.MenuItemOrdenAsc.Click += new System.EventHandler(this.MenuItemOrdenAsc_Click);
            // 
            // MenuItemOrdenDesc
            // 
            this.MenuItemOrdenDesc.Name = "MenuItemOrdenDesc";
            this.MenuItemOrdenDesc.Size = new System.Drawing.Size(142, 22);
            this.MenuItemOrdenDesc.Text = "Descendente";
            this.MenuItemOrdenDesc.Click += new System.EventHandler(this.MenuItemOrdenDesc_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(139, 6);
            // 
            // MenuItemOrdenNone
            // 
            this.MenuItemOrdenNone.Checked = true;
            this.MenuItemOrdenNone.CheckState = System.Windows.Forms.CheckState.Checked;
            this.MenuItemOrdenNone.Name = "MenuItemOrdenNone";
            this.MenuItemOrdenNone.Size = new System.Drawing.Size(142, 22);
            this.MenuItemOrdenNone.Text = "Ninguna";
            this.MenuItemOrdenNone.Click += new System.EventHandler(this.MenuItemOrdenNone_Click);
            // 
            // TextoBusqueda
            // 
            this.TextoBusqueda.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.TextoBusqueda.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.TextoBusqueda.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.RecentlyUsedList;
            this.TextoBusqueda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TextoBusqueda.ForeColor = System.Drawing.Color.Gray;
            this.TextoBusqueda.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.TextoBusqueda.Name = "TextoBusqueda";
            this.TextoBusqueda.Size = new System.Drawing.Size(100, 23);
            this.TextoBusqueda.Text = "Búsqueda";
            this.TextoBusqueda.ToolTipText = "Archivo a buscar...";
            this.TextoBusqueda.Leave += new System.EventHandler(this.TextoBusqueda_Leave);
            this.TextoBusqueda.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TextoBusqueda_MouseDown);
            this.TextoBusqueda.TextChanged += new System.EventHandler(this.TextoBusqueda_TextChanged);
            // 
            // VentanaPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(697, 326);
            this.Controls.Add(this.PanelMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.BaraMenu;
            this.Name = "VentanaPrincipal";
            this.Text = "Explorador de Archivos";
            this.PanelMenu.ContentPanel.ResumeLayout(false);
            this.PanelMenu.TopToolStripPanel.ResumeLayout(false);
            this.PanelMenu.TopToolStripPanel.PerformLayout();
            this.PanelMenu.ResumeLayout(false);
            this.PanelMenu.PerformLayout();
            this.PanelSeparador.Panel1.ResumeLayout(false);
            this.PanelSeparador.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PanelSeparador)).EndInit();
            this.PanelSeparador.ResumeLayout(false);
            this.BaraMenu.ResumeLayout(false);
            this.BaraMenu.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer PanelMenu;
        private System.Windows.Forms.SplitContainer PanelSeparador;
        private System.Windows.Forms.TreeView PanelArbolDirectorio;
        private System.Windows.Forms.ListView ListadoArchivos;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ImageList ImageListSmall;
        private System.Windows.Forms.MenuStrip BaraMenu;
        private System.Windows.Forms.ToolStripMenuItem MenuVista;
        private System.Windows.Forms.ToolStripMenuItem MenuItemIconos;
        private System.Windows.Forms.ToolStripMenuItem MenuItemIconosPeq;
        private System.Windows.Forms.ToolStripMenuItem MenuItemIconosGran;
        private System.Windows.Forms.ToolStripMenuItem MenuItemLista;
        private System.Windows.Forms.ToolStripMenuItem MenuItemDetalle;
        private System.Windows.Forms.ToolStripMenuItem MenunArchivo;
        private System.Windows.Forms.ToolStripMenuItem MenuItemSalir;
        private System.Windows.Forms.ImageList ImageListBig;
        private System.Windows.Forms.Label LabelCargando;
        private System.Windows.Forms.ToolStripTextBox TextoBusqueda;
        private System.Windows.Forms.ToolStripMenuItem MenuItemCuadricula;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem MenuItemOrdenacion;
        private System.Windows.Forms.ToolStripMenuItem MenuItemOrdenAsc;
        private System.Windows.Forms.ToolStripMenuItem MenuItemOrdenDesc;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem MenuItemOrdenNone;
    }
}

