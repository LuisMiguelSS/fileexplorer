﻿
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace ExploradordeArchivos
{
    static class FileManager
    {
        public static string GetSizeString(long FileSize)
        {
            if (FileSize == 0)
                return "Empty";
            else if (FileSize < 1024)
                return FileSize + " bytes";
            else if (FileSize < 1048576)
                return FileSize / 1024 + " Kb";
            else if (FileSize < 1073741824)
                return FileSize / 1048576 + " Mb";
            else if (FileSize < 1099511627776)
                return FileSize / 1073741824 + " Gb";
            else if (FileSize < 1125899906842624)
                return FileSize / 1099511627776 + " Tb";
            else
                return "> 1000 Tb";
        }

        public static string GenerateKeyForIcon(string FilePath)
        {
            return new FileInfo(FilePath).Name;
        }
        public static Icon GetIcon(string FilePath)
        {
            return Icon.ExtractAssociatedIcon(FilePath);
        }
        public static bool HasIcon(string FilePath)
        {
            return GetIcon(FilePath) != null;
        }


        /**
         * Dado un archivo, intenta obtener una previsualización del mismo como
         * icono para que el usuario pueda distinguir rápidamente qué imagen es.
         */
        public static Icon GetImageFileThumbnail(string FilePath)
        {
            Cursor.Current = Cursors.WaitCursor;

            Bitmap bitmap = new Bitmap(FilePath);
            IntPtr newIcon = bitmap.GetHicon();
            Icon i = Icon.FromHandle(newIcon);

            i.Dispose();
            Cursor.Current = Cursors.Default;
            return i;
        }
    }
}
